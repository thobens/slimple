module.exports = {
    port: process.env.VCAP_APP_PORT || process.env.PORT || 8080,
    serviceLocation: '/boilerplate/services',
    eventLocation: '/boilerplate/events',
    contentLocation: '/boilerplate/content',
    schemaLocation: '/boilerplate/schemas',
    db: require('./db'),
    log: require('./log')
};