var navigationItems = {
    brand:{
        name: "Slimple Boilerplate",
        href: "pages/testPage.html"
    },
    items: [{
        name: "Test",
        href: "pages/testPage.html",
        items: []
    }]    
};
var navigation = {
    name: 'navigation',
    renderTo: 'navigationPanel',
    template: '/cmp/navigation/navTemplate.ejs',
    staticData: navigationItems,
    listeners: {
        editButton: {
            element: '.navItem',
            event: 'click',
            invoke: function(id, cmp, event) {
                $('#contentPanel').load(event.target.href);
            }
        }
    }
};