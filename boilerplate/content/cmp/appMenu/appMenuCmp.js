var appMenuConfig = {
    title: 'Main',
    items: [{
        name: 'Dashboard',
        icon: 'icon-home',
        href: 'pages/testPage.html'
    },{
        name: 'Stream',
        icon: 'icon-comment',
        href: 'pages/testPage.html'
    },{
        name: 'Blogs',
        icon: 'icon-edit',
        href: 'pages/blog.html'
    },{
        name: 'Calendar',
        icon: 'icon-calendar',
        href: 'pages/testPage.html'
    },{
        name: 'Galleries',
        icon: 'icon-picture',
        href: ''
    },{
        name: 'Members',
        icon: 'icon-user',
        href: 'pages/testPage.html'
    }]
};
var appMenu = {
    name: 'appMenu',
    renderTo: 'leftPanel',
    template: '/cmp/appMenu/appMenuTemplate.ejs',
    staticData: appMenuConfig,
    listeners: {
        editButton: {
            element: '.appMenuItem',
            event: 'click',
            invoke: function(id, cmp, event) {
                $(".active.appMenuLi").removeClass("active");
                $(event.target).parent().addClass("active");
                $('#contentPanel').load(event.target.href);
            }
        }
    }
};