var App = function(){
    this.viewManager = new Sui.view.Manager({
        views: [
            new Sui.view.Component(navigation),
            new Sui.view.Component(userMenu),
            new Sui.view.Component(appMenu),
            new Sui.view.Component(signup)
        ]
    });
    this.viewManager.build();
};
