/**
 * simple echo service
 */
module.exports = function(serviceHolder) {
    
    // define service endpoint
    serviceHolder['/echo'] = function(ctx, data, http) {

        // return the service methods
        return {
            GET: function(){
                ctx.event.emit("echoEvent", ctx.session.uid()+" > "+http.request.method + " echo event: " + data.text);
                ctx.extCtxMth("extended context method called from service");
                http.returnJson(data);
            },
            POST: function(){},
            PUT: function(){},
            DELETE: function() {}
            
        };

    };
};