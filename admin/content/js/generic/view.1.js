function GenericView(schema){
    
    var store = new Sui.data.Generic(schema);
    var viewManager = new Sui.view.Manager({
        views: [
            new Sui.view.Component({
                name: schema+'List',
                type: 'list',
                renderTo: schema+'ListDiv',
                template: 'pages/template/generic/list.ejs',
                dataStore: store,
                listeners: {
                    editButton: {
                        element: '.editButton',
                        event: 'click',
                        invoke: function(id, cmp){
                            cmp.manager.getView(schema+'Form').loadData(id);
                        }
                    },
                    deleteButton: {
                        element: '.deleteButton',
                        event: 'click',
                        invoke: function(id, cmp){
                            cmp.deleteData(id, function(response){
                                if(!response.successful){
                                    Sui.util.processResponse(response);
                                } else {
                                    cmp.manager.getView(schema+'Form').resetData();
                                    cmp.loadData();
                                }
                            });
                        }
                    }
                }
            }),
            new Sui.view.Component({
                name: schema+'Form',
                type: 'form',
                renderTo: schema+'FormDiv',
                template: 'pages/template/generic/form.ejs',
                dataStore: store,
                listeners: {
                    saveButton: {
                        element: '#saveButton',
                        event: 'click',
                        invoke: function(id, cmp){
                            cmp.saveData(function(response){
                                if(!response.successful){
                                    Sui.util.processResponse(response);
                                } else {
                                    cmp.manager.getView(schema+'List').loadData();
                                    cmp.resetData();
                                }
                            });
                        }
                    },
                    resetButton: {
                        element: '#resetButton',
                        event: 'click',
                        invoke: function(id, cmp){
                            cmp.resetData();
                        }
                    }
                }
            })
        ]
    });
    
    viewManager.build();
}