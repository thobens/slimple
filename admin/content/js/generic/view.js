function GenericView(schema) {

    var store = new Sui.data.Generic(schema);
    /* var config = {
        schema: schema,
        schemaService: '/schemas/get',
        readAllService: '/generic/list',
        readService: '/generic/read',
        saveService: '/generic/save',
        deleteService: '/generic/delete'
    };
    var store = new Sui.data.Generic2(config);*/
    var viewManager = new Sui.view.Manager({
        views: [
        new Sui.view.Component({
            name: schema + 'List',
            type: 'list',
            renderTo: 'GenericListPanel',
            template: 'pages/template/generic/list.ejs',
            dataStore: store,
            listeners: {
                editButton: {
                    element: '.editButton',
                    event: 'click',
                    invoke: function(id, cmp) {
                        cmp.manager.getView(schema + 'Form').loadData(id, function() {
                            $('#detailDialog').modal('show');
                        });
                    }
                },
                deleteButton: {
                    element: '.deleteButton',
                    event: 'click',
                    invoke: function(id, cmp) {
                        cmp.deleteData(id, function(response) {
                            if (!response.successful) {
                                Sui.util.processResponse(response);
                            }
                            else {
                                var searchString = $('#searchField').val();
                                if (searchString !== undefined && searchString.length > 0) {
                                    var searchType = $('#searchType').val();
                                    var query = '{"' + searchType + '" : "' + searchString + '"}';
                                    cmp.find(query, function(response) {
                                        cmp.manager.getView(schema + 'Form').resetData();
                                    });
                                }
                                else {
                                    cmp.loadData(null, function() {
                                        cmp.manager.getView(schema + 'Form').resetData();
                                    });

                                }
                            }
                        });
                    }
                },
                searchButton: {
                    element: '#searchButton',
                    event: 'click',
                    invoke: function(id, cmp) {
                        var searchType = $('#searchType').val();
                        var searchString = $('#searchField').val();
                        if (searchString) {
                            var query = '{"' + searchType + '" : "' + searchString + '"}';
                            cmp.find(query, function(response) {});
                        }
                        else {
                            cmp.manager.getView(schema + 'List').loadData(null, function() {});
                        }
                    }
                }
            }
        }),
        new Sui.view.Component({
            name: schema + 'Form',
            type: 'form',
            renderTo: 'GenericFormPanel',
            template: 'pages/template/generic/form.ejs',
            dataStore: store,
            functions: {
                resetComponent: function(cmp) {
                    cmp.resetData();
                    $('#detailDialog').modal('hide');
                    $('.modal-backdrop').remove(); // hack to remove the stupid backdrop
                }
            },
            listeners: {
                saveButton: {
                    element: '#saveButton',
                    event: 'click',
                    invoke: function(id, cmp) {
                        cmp.saveData(function(response) {
                            if (!response.successful) {
                                Sui.util.processResponse(response);
                            }
                            else {

                                var searchString = $('#searchField').val();
                                if (searchString !== undefined && searchString.length > 0) {
                                    var searchType = $('#searchType').val();
                                    var query = '{"' + searchType + '" : "' + searchString + '"}';
                                    cmp.manager.getView(schema + 'List').find(query, function(response) {
                                        cmp.functions.resetComponent(cmp);
                                    });
                                }
                                else {
                                    cmp.manager.getView(schema + 'List').loadData(null, function() {
                                        cmp.functions.resetComponent(cmp);
                                    });

                                }

                            }
                        });
                    }
                },
                resetButton: {
                    element: '#resetButton',
                    event: 'click',
                    invoke: function(id, cmp) {
                        cmp.resetData();
                    }
                }
            }
        }),
        new Sui.view.Component({
            name: schema + 'Search',
            type: 'form',
            renderTo: 'GenericSearchPanel',
            template: 'pages/template/generic/search.ejs',
            dataStore: store,
            listeners: {
                searchButton: {
                    element: '#searchField',
                    event: 'keyup',
                    invoke: function(id, cmp) {
                        var searchType = $('#searchType').val();
                        var searchString = $('#searchField').val();
                        if (searchString) {
                            var query = '{"' + searchType + '" : "' + searchString + '"}';
                            cmp.manager.getView(schema + 'List').find(query, function(response) {});
                        }
                        else {
                            cmp.manager.getView(schema + 'List').loadData(null, function() {});
                        }
                    }
                }
            }
        })]
    });

    viewManager.build();
}