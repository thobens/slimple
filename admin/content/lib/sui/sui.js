/**
 *  Slimple UI and AJAX Toolkit
 */
var Sui = {

    ready: function(callback){
        document.addEventListener("DOMContentLoaded", function() {
            callback();
        }, false);
    },

    /**
     * utilities for data conversion and stuff.
     */
    util: {
        /**
         * serialize a flat object.
         * no nested objects possible yet.
         * TODO make this function recursiv for nested objects
         */
        serialize: function(obj) {
            var str = [];
            for(var p in obj){
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
            return str.join("&");
        },
        convertFormToJson: function(form){
            var data = {};
            for (var i = 0, ii = form.length; i < ii; ++i) {
                var input = form[i];
                if (input.name) {
                  data[input.name] = input.value;
                }
            }
            return data;
        },

        serializeForm: function(form){
            var data = Sui.util.convertFormToJson(form);
            return JSON.stringify(data);
        },
        processResponse: function(returnObject){
            if(returnObject.successful){                                
                //window.location.reload();
            } else {
                var msgStr = 'Required: \n';
                for(var k in returnObject.message){
                    msgStr += ' '+k+', ';
                }
                msgStr = msgStr.substring(0, msgStr.length - 2);
                //JSON.stringify(returnObject.message)
                alert(msgStr);
            }
        }
    },
    /**
     * http functions
     */
    http: {

        /**
         * ajax request
         * configuration object must have following structure:
         * {
         *      method: <'GET', 'POST', whatever >,
         *      endpoint: <URL to the service endpoint>,
         *      async: <true or false>,
         *      data: <json object with data>
         *  }
         * TODO synchronous calls
         *
         */
        ajax: function(config, callback){

            var serializedData = Sui.util.serialize(config.data);
            var endPointUrl = config.method === 'GET' || config.method === 'DELETE' ? config.endpoint+'?'+serializedData : config.endpoint;
            var postData = config.method === 'POST' || config.method === 'PUT' ? serializedData : null;

            var http = new XMLHttpRequest();
            http.open(config.method, endPointUrl, config.async);
            http.setRequestHeader("X-cache","none");
            http.onreadystatechange = function () {
                if (http.readyState == 4) {
                    var responseTxt = http.responseText;
                    var returnJson = eval('(' + responseTxt + ')');
                    callback(returnJson, http.status);
                }
            };
            http.send(postData);
        }
    },

    /**
     *  tools for building generic views 
     */
    view: {
        
        Manager: function(config){
            var that = this;
            var views = [];
            
            this.getView = function(name){
                return views[name];
            };
            
            this.build = function(){
                for(var i = 0; i < config.views.length; i++){
                    config.views[i].build(that);
                    views[config.views[i].name] = config.views[i];
                }
            };
            
        },
        
        Component: function(config){
            // public
            this.name = config.name;
            this.manager = {};
            
            // private
            var that = this;
            var type = config.type;
            var renderTo = config.renderTo;
            var template = new EJS({
                url : config.template+'?dt=' + Date.now()
			});
            var dataStore = config.dataStore;
            var listeners = config.listeners;
            var dataModel = {
                name: config.name,
                schema: {},
                data: {}
            };

            this.functions = config.functions ? config.functions : {};

            this.setName = function(newName){
                this.name = newName;
            };
    
            this.addListeners = function(listeners){
                
               for(var listener in listeners){
                    var currentListener = listeners[listener];
                    var elements = document.querySelectorAll(currentListener.element);
                    for(var i = 0; i < elements.length; i++){
                        elements.item(i).addEventListener(currentListener.event,(function(eventListener, cmp) {
                            return function(event){
                                event.preventDefault();
                                var recordId = event.target.getAttribute("recordId");
                                eventListener.invoke(recordId, cmp);
                            };
                        })(currentListener, that));
                        
                    }
                } 
            };

            this.build = function(componentManager){
                this.manager = componentManager;
                if(dataStore){
                    dataStore.schema(function(schemaDefinition){
                        dataModel.schema = schemaDefinition;
                        if(type === 'list'){
                            dataStore.readAll(function(listData){
                                dataModel.data = listData;    
                                template.update(renderTo, dataModel);
                                that.addListeners(listeners);
                            });
                        } else {
                            template.update(renderTo, dataModel);
                            that.addListeners(listeners);
                        }
                    }); 
                }
                    
                    
            };
                  
            this.find = function(query, callback){
                dataStore.find(query, function(result){
                    dataModel.data = result;
                    template.update(renderTo, dataModel);
                    that.addListeners(listeners);
                    if(callback){callback(dataModel);}                  
                });
            };
            this.loadData = function(id, callback){
                if(id){
                    dataStore.read(id, function(result){
                        dataModel.data = result;
                        template.update(renderTo, dataModel);
                        that.addListeners(listeners);
                        if(callback){callback();}
                    });
                } else {
                    dataStore.readAll(function(result){
                        dataModel.data = result;
                        template.update(renderTo, dataModel);
                        that.addListeners(listeners);
                        if(callback){callback();}
                    });
                }
                
            };
            
            this.resetData = function(){
                dataModel.data = {};
                template.update(renderTo, dataModel);
                that.addListeners(listeners);
            };
            
            this.saveData = function(callback){
                var form = document.getElementById(dataModel.name);
                var data = Sui.util.serializeForm(form);
                dataStore.save(data, function(response){
                    callback(response);
                });
            };
                        
            this.deleteData = function(id, callback){ 
                dataStore.delete(JSON.stringify({_id:id}), function(response){
                    callback(response);
                });
            };
 
        }
        
    },
        
    /**
     * data access
     */ 
    data: {
        
        Store: function(config){
            
            var that = this;
            
            this.model = config.model;
            
            this.service = config.endpoint+'/'+this.model;
            
            this.create = function(data, callback){
                that.exec({
                    method: 'POST',
                    service: this.service,
                    data: data
                }, function(response){
                    callback(response);
                });
            };
            
            this.read = function(id, callback){
                that.exec({
                    method: 'GET',
                    service: id ? this.service+'/'+id : this.service
                }, function(response){
                    callback(response);
                });
            };
            
            this.update = function(data, callback){
                that.exec({
                    method: 'PUT',
                    service: this.service+'/'+data.id,
                    data: data
                }, function(response){
                    callback(response);
                });
            };
            
             this.delete = function(id, callback){
                that.exec({
                    method: 'DELETE',
                    service: this.service+'/'+id
                }, function(response){
                    callback(response);
                });
            };
                       
            
            this.exec = function(config, callback){
                Sui.http.ajax({
                    method: config.method,
                    endpoint: config.service,
                    async: true,
                    data: {
                        data: config.data,
                        timestamp: Date.now()
                    }
                }, function(data){
                    callback(data);
                });
            };
            
            
        },
        
        /**
         * generict data access over ajax
         */ 
        Generic: function(schema){
            return {
                
                currentDate: Date.now(),
                schemaName: schema,
                schemaModel: {},
                
                config: {
                    schemaService: '/schemas/get',
                    findService: '/generic/find',
                    readAllService: '/generic/list',
                    readService: '/generic/read',
                    saveService: '/generic/save',
                    deleteService: '/generic/delete'
                },
                schema: function(callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'GET',
                        endpoint: this.config.schemaService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            date: that.currentDate
                        }
                    },function(data){
                        that.schemaModel = data;
                        callback(data);
                    });
                   
                },
                find: function(findQuery, callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'GET',
                        endpoint: that.config.findService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            query: findQuery,
                            date: that.currentDate
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
                readAll: function(callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'GET',
                        endpoint: that.config.readAllService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            date: that.currentDate
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
                read: function(recordId, callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'GET',
                        endpoint: that.config.readService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            id: recordId,
                            date: that.currentDate
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
                save: function(dataObject, callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'POST',
                        endpoint: that.config.saveService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            data: dataObject,
                            date: that.currentDate
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
               
                delete: function(dataObject, callback){
                    var that = this;
                    Sui.http.ajax({
                        method: 'DELETE',
                        endpoint: that.config.deleteService,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            data: dataObject,
                            date: that.currentDate
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                }                
            
            };

        },
        Generic2: function(config){
            return {
                
                schemaName: config.schema,
                store: new Sui.data.Store(config),
                schemaModel: {},
                
                schema: function(callback){
                    var that = this;
                    that.store.exec({
                        method: 'GET',
                        endpoint: that.store.services.schema,
                        data: {
                            schema: that.schemaName
                        }
                    },function(data){
                        that.schemaModel = data;
                        callback(data);
                    });
                   
                },
                
                readAll: function(callback){
                    var that = this;
                   that.store.exec({
                        method: 'GET',
                        endpoint: that.store.services.readAll,
                        async: true,
                        data: {
                            schema: that.schemaName
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
                read: function(recordId, callback){
                    var that = this;
                     that.store.exec({
                        method: 'GET',
                        endpoint: that.store.services.read,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            id: recordId
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
                save: function(dataObject, callback){
                    var that = this;
                    that.store.exec({
                        method: 'POST',
                        endpoint: that.store.services.save,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            data: dataObject
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                },
               
                delete: function(dataObject, callback){
                    var that = this;
                    that.store.exec({
                        method: 'POST',
                        endpoint: that.store.services.delete,
                        async: true,
                        data: {
                            schema: that.schemaName,
                            data: dataObject
                        }
                    }, function(returnData, status){
                        callback(returnData);
                    });
                }                
            
            };

        }
    }

};