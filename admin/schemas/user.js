module.exports = {
    name: 'User',
    schema: {
        name : { type : "String", required : true },
        password : { type : "String", required : true, encrypted: true },
        email : { type : "String", required : true },
        birthdate : { type : "String", required : true },
        country : { type : "String", required : true },
        city : { type : "String", required : true }
    }
};