var crypto = require('crypto');

module.exports = function(services) {

    services['/db/:schema(/:id)'] = function(ctx, data, http) {

        var id = data.id;
        var schema = data.schema;
        var schemaDefinition = ctx.db.getSchema(schema);
        
        return {
            
            /**
             * read
             */
            GET: function(){
                ctx.db.perform(function(db) {
                    var model = ctx.db.model(schema);
                    if(id){ // read by id
                        model.findById(id, function(error, result) {
                            http.returnJson(result);
                        });
                    } else { // read all
                        model.find().execFind(function(error, result) {
                            http.returnJson(result.reverse()); // FIXME create a solution of last to oldest
                        });
                    }
                });
            },
            
            /**
             * create entry
             */
            POST: function(){
                ctx.db.perform(function(db) {
                    var Model = ctx.db.model(schema);
                    var preparedData = mapData(schemaDefinition, data, {});
                    var dbObject = new Model(preparedData);
                    // save
                    dbObject.save(function(error) {
                        http.returnJson(error ? {msg:error} : {});
                    });
                }); 
            },
            
            /**
             * modify entry 
             */
            PUT: function(){
                if(id){
                    ctx.db.perform(function(db) {
                        var Model = ctx.db.model(schema);
                        Model.findOne({
                            _id: id
                        }, function(err, doc) {
                            var upd = mapData(schemaDefinition, data, doc);
                            upd.save(function(error) {
                                http.returnJson(error ? {msg:error} : {});
                            });
                        });
                    });
                } else {
                    http.returnJson({msg:'no id specified'});
                }
            },
            /**
             * delete entry
             */
            DELETE: function() {
                if(id){
                    ctx.db.perform(function(db) {
                        var model = ctx.db.model(schema);
                        model.remove({
                            _id: id
                        }, function(error) {
                            http.returnJson(error ? {msg:error} : {});
                        });
                    });
                } else {
                    http.returnJson({msg:'no id specified'});
                }
            }
            
        };

    };
    
    var mapData = function(schema, from, to) {
        delete from._id;
        delete from.schema;
        for (var key in from) {
            if (from[key] !== null && from[key] !== 'undefined') {
                if(schema[key].encrypted){ // TODO map of additional properties
                    if(!to[key]){ // encrypt only if field didn't containi any data yet (insert)
                        var md5 = crypto.createHash('md5').update(from[key]).digest("hex");
                        to[key] = md5;
                    }
                } else {
                    to[key] = from[key];
                }
            }
        }
        return to;
    };
};