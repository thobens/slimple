var msg = require(process.cwd() + "/module/util/json/message");
var crypto = require('crypto');

/**
 *
 */
module.exports = function(serviceHolder) {

    var mapData = function(schema, from, to) {
        delete from._id;
        delete from.schema;
        for (var key in from) {
            if (from[key] !== null && from[key] !== 'undefined') {
                if(schema[key].encrypted){ // TODO map of additional properties
                    if(!to[key]){ // encrypt only if field didn't containi any data yet (insert)
                        var md5 = crypto.createHash('md5').update(from[key]).digest("hex");
                        to[key] = md5;
                    }
                } else {
                    to[key] = from[key];
                }
            }
        }
        return to;
    };

    var responseError = function(val, ctx) {

        if (val !== null && val.errors !== null) {
            return msg.success(val === null, val.errors); //err.errors.title.type));
        }
        else {
            return msg.success(val === null, {});
        }

    };

    serviceHolder['/generic/find'] = function(ctx, data, http) {
        return {
            GET: function() {
                var schemaName = data.schema;
                var query = JSON.parse(data.query);
                ctx.db.perform(function(db) {
                    var model = ctx.db.model(schemaName);

                    for (var k in query) {
                        query[k] = new RegExp(query[k], 'i');
                    }

                    model.find(query).execFind(function(error, list) {
                        http.returnJson(list.reverse()); // FIXME create a solution of last to oldest
                    });
                });
            }
        };
    };

    serviceHolder['/generic/list'] = function(ctx, data, http) {
        return {
            GET: function() {
                var schemaName = data.schema;
                ctx.db.perform(function(db) {
                    var model = ctx.db.model(schemaName);
                    model.find().execFind(function(error, list) {
                        http.returnJson(list.reverse()); // FIXME create a solution of last to oldest
                    });
                });
            }
        };
    };

    serviceHolder['/generic/read'] = function(ctx, data, http) {
        return {
            GET: function() {
                var schemaName = data.schema;
                var recordId = data.id;
                ctx.db.perform(function(db) {
                    var model = ctx.db.model(schemaName);
                    model.findById(recordId, function(error, list) {
                        http.returnJson(list); // FIXME create a solution of last to oldest
                    });
                });

            }
        };
    };

    serviceHolder['/generic/delete'] = function(ctx, data, http) {
        return {
            DELETE: function() {
                var schemaName = data.schema;
                var dataObject = JSON.parse(data.data);
                var recordId = dataObject._id;

                ctx.db.perform(function(db) {
                    var model = ctx.db.model(schemaName);
                    model.remove({
                        _id: recordId
                    }, function(err) {
                        http.returnJson(responseError(err, ctx));
                    });
                });
            }
        };
    };

    serviceHolder['/generic/save'] = function(ctx, data, http) {
        return {
            POST: function() {
                var schemaName = data.schema;
                var dataObject = JSON.parse(data.data);
                var recordId = dataObject._id;

                ctx.db.perform(function(db) {
                    var schema = ctx.db.getSchema(schemaName);
                    
                    var Model = ctx.db.model(schemaName);

                    if (recordId) { // update
                        Model.findOne({
                            _id: recordId
                        }, function(err, doc) {
                            var upd = mapData(schema, dataObject, doc);
                            upd.save(function(val) {
                                http.returnJson(responseError(val, ctx));
                            });
                        });
                    }
                    else { // insert
                        var preparedData = mapData(schema, dataObject, {});
                        var dbObject = new Model(preparedData);

                        // save
                        dbObject.save(function(val) {
                            http.returnJson(responseError(val, ctx));
                        });
                    }

                });

            }
        };


    };

};