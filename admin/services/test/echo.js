/**
 * simple echo service
 */
module.exports = function(serviceHolder) {
    
    // define service endpoint
    serviceHolder['/echo/:text'] = function(ctx, data, http,paramMap) {

        // return the service methods
        return {
            GET: function(){
                console.log(paramMap);
                ctx.event.emit("echoEvent", ctx.session.uid()+" > "+http.request.method + " echo event: " + data.text);
                ctx.extCtxMth("extended context method called from service");
                http.returnJson(data);
            },
            POST: function(){},
            PUT: function(){},
            DELETE: function() {}
            
        };

    };
};