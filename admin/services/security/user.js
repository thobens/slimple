var crypto = require('crypto');

module.exports = function(serviceHolder) {

    serviceHolder['/user/auth'] = function(ctx, data, http) {
        return {
            PUT: function() {
                ctx.db.perform(ctx, function(db) {
                    var Model = ctx.db.model(db, "User");
                    Model.findOne({
                        name: data.user
                    }, function(err, foundUser) {
                        if (!foundUser) {
                            http.returnJson({
                                isLogin: false,
                                error: err
                            });
                        }
                        else {
                            var pwHash = crypto.createHash('md5').update(data.password).digest("hex");
                            if (data.user == foundUser.name && pwHash == foundUser.password) {
                                ctx.session.set("user", data.user);
                                http.returnJson({
                                    isLogin: true
                                });
                            }
                            else {
                                http.returnJson({
                                    isLogin: false,
                                    error: 'Wrong Username or Password'
                                });
                            }
                        }

                    });
                });
            },
            GET: function() {
                http.returnJson({
                    sid: ctx.session.uid(),
                    user: ctx.session.get("user")
                });
            },
            POST: function() {},
            DELETE: function() {}

        };
    };

    serviceHolder['/user'] = function(ctx, data, http) {
        ctx.db.perform(ctx, function(db) {
            var Model = ctx.db.model(db, "User");
            return {
                GET: function() {},
                POST: function() {
                    if (data._id) { // update
                        Model.findOne({
                            _id: data._id
                        }, function(err, existingRecord) {
                            var upd = ctx.util.json.mapData(data, existingRecord);
                            upd.save(function(val) {
                                http.returnJson(val);
                            });
                        });
                    }
                },
                PUT: function() {
                    var preparedData = ctx.util.json.mapData(data, {});
                    var dbObject = new Model(preparedData);
                    dbObject.save(function(val) {
                        http.returnJson(val);
                    });
                },
                DELETE: function() {}
            };
        });
    };

};