module.exports = {
    name: 'BlogPost',
    schema: {
        author  : { type: "String", required: true},
        title   : { type: "String", required: true},
        body    : { type: "String", required: true},
        date    : { type: "Date", default: new Date() }
    }
};