var os = require("os");

/**
 * Service for server related tasks
 */ 
module.exports = function(serviceHolder) {

    serviceHolder['/serverInfo'] = function(ctx,data, http){
        return {
            GET: http.returnJson({
                    platform : os.type() + " / "+ os.release(),
                    arch : os.arch(),
                    hostname : os.hostname(),
                    uptime : os.uptime(),
                    totalmem : os.totalmem(),
                    freemem : os.freemem()
                })
        };
    };

    /**
     * returns all active service names
     */ 
    serviceHolder['/services'] = function(ctx,data, http){
        return {
            GET: http.returnJson({services : Object.keys(ctx.services)})
        };
    };
    /**
     * returns all loaded mongoDB schemas
     */ 
    serviceHolder['/schemas'] = function(ctx,data, http){
        return {
            GET: http.returnJson(ctx.db.schemas)
        };
    };
};