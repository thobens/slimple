//var mongoose = require('mongoose');
var walker = require("../../util/file/treeWalker");
var schemaHolder = [];

exports.loadSchemas = loadSchema;
exports.schemaHolder = schemaHolder;

/**
 * Load all schemas in a given path recursively.
 */
function loadSchema(ctx) {
    var path = process.cwd()+ctx.config.schemaLocation;
    walker.exec(path, function(file){
        ctx.log.info('load schema: ' + file);
        var descriptor = require(file);
        schemaHolder[descriptor.name] = descriptor; 
       //var dbSchema = new ctx.db.mongoose.Schema(descriptor.schema);
    });
    return schemaHolder;
}


