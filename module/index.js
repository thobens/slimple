module.exports = {
    context: require('./core/context'),
    server: require('./core/server'),
    logAppenders: [
        require('./log/consoleAppender.js')
        //require('./log/fileAppender.js')
    ]
};