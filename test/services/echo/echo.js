module.exports = {
    
    // the resource where this service is available 
    //resource: '/echo(/:what)',
    //resource: '(/:what)',
    /**
     * handler for GET request
     */ 
    GET: function(ctx, data, http){
        
        //http.response.setHeader('Content-Type','application/json');
        //this.anotherServiceMethod(ctx,data,http);
        if(data.what === 'error'){
            throw { status: 500, error : new Error('asdw').stack };
        }
        http.reply('service 0' + JSON.stringify(data));
    },
    
    /**
     * handler for POST request
     */
    POST: function(ctx, data, http){
        http.reply(data);
    },
    
    // possible handlers: GET, POST, PUT, DELETE
    
    /**
     *  a method to show some Slimple functionalities
     */ 
    anotherServiceMethod: function(ctx, data, http){
        
        // session variables
        var sessionId = http.session.uid();
        var counter = http.session.get('counter');
        counter = counter ? counter++ : 1;
        http.session.set('counter', counter);
        
        // logging: trace, debug, info, error
        ctx.log.trace(sessionId+" > counter = "+counter);
        
        // event emit
        ctx.event.emit("echoEvent", sessionId+" > "+http.request.method + " echoEvent: " + JSON.stringify(data));
        
        // user defined method in the context, see app.js
        ctx.customContextMethod(sessionId+" > "+"extended context method called from echo service");
    }
};
