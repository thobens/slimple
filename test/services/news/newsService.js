module.exports = {
    
    resource: '(/:newsId)',

    GET: function(ctx, data, http){
        http.reply('news ' + JSON.stringify(data));
    }
};
