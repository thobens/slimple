module.exports = {
    
    resource: '(/:newsId)/statistics(/:statisticId)',

    GET: function(ctx, data, http){
        http.reply('statistics ' + JSON.stringify(data));
    }
};
