module.exports = {
    
    resource: '(/:commentId)',

    GET: function(ctx, data, http){
        http.reply('comments ' + JSON.stringify(data));
    }
};
