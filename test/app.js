var config = require(process.cwd()+'/test/config/app');
var slimple = require(process.cwd()+'/module/index'); 
//var slimple = require("slimple"); 
//var fs = require('fs');

/**
 * minimal app skeleton
 */ 
var app = {

    /**
     * initialize a custom app context
     */ 
    context : {
        customContextMethod : function(text){
            this.log.trace(text); // methods from the slimple app context are available in the extended context
        } 
    },
    
    /**
     * initialize the app
     */ 
    init: function(){
        // delete logfile
        // TODO not just delete => copy and rename. implement this in log4node init method
        //fs.unlink(process.cwd()+"/log/server.log", function(err){ err !== null ? console.log(err) : ""});
        // build context
        //this.context = slimple.contextFactory.build(config, this.context);
        this.context = slimple.context.build(config, this.context);
    },
    
    /**
     * run the app
     */ 
    run: function(){
        this.init();
        slimple.server.httpd.start(this.context);
    }
    
};

app.run();
