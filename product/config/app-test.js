module.exports = {
    port: process.env.VCAP_APP_PORT || process.env.PORT || 8080,
    serviceLocation: '/product/services',
    eventLocation: '/product/events',
    contentLocation: '/product/content',
    schemaLocation: '/product/schemas',
    db: require('./db'),
    log: require('./log')
};