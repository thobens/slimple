var config = require('./config/app.js');
//var slimple = require(process.cwd()+'/module/index'); 
var slimple = require("slimple"); 
var fs = require('fs');

/**
 * minimal app skeleton
 */ 
var app = {

    /**
     * initialize a custom app context
     */ 
    context : {
        extCtxMth : function(text){
            this.log.trace(text); // methods from the context are available in the extended context
        } 
    },
    
    /**
     * initialize the app
     */ 
    init: function(){
        // delete logfile
        // TODO not just delete => copy and rename. implement this in log4node init method
        //fs.unlink(process.cwd()+"/log/server.log", function(err){ err !== null ? console.log(err) : ""});
        // build context
        this.context = slimple.buildContext(config, this.context);
    },
    
    /**
     * run the app
     */ 
    run: function(){
        this.init();
        slimple.server.httpd.start(this.context);
    }
    
};

app.run();
