/**
* simple echo service for REST request and JSON response handler
*/
module.exports = function(serviceHolder) {
    // define service 
    serviceHolder['/echo'] = function(ctx,request,response){
        // handle request, pass the given data directly to callback
        ctx.web.requestHandler.rest(request, response, function(data){
            ctx.event.emit("echoEvent", "echo event emit: "+data.text);
            ctx.extCtxMth("extended context method called");
            // respond in JSON with given data
            ctx.web.responseHandler.writeJson(response, data);
        });	
    };
};